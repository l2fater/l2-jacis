# L2Fater - L2JACIS -INTERLUDE 天堂2铁幕降临六章中文源码
[![Website www.l2fater.cn](https://img.shields.io/website-up-down-green-red/https/www.l2fater.cn.svg)](https://www.l2fater.cn/)
[![made-with-java](https://img.shields.io/badge/Made%20with-Java-1791ce.svg)](https://java.com/)

## 简介 - Description

这是 L2JACIS 服务器模拟器的一个分支。
这是一个闭源项目，与`天堂2六章：铁幕降临INTERLUDE`客户端合作。 
主要用 Java 编写。

## 项目现状 - Project status

项目属于间断性更新。  L2Fater提供汉化及部分Bug修改

## 搭建所需环境 - Prerequisites
- Java 11

- MySQL or MariaDB
- Git

**Note:** 集成开发环境可能已经包含了其中的一些内容。

## Bug 提交 - Bug Support

访问血玫瑰社区 [论坛]https://www.l2fater.cn 进行发帖交流
visit [forum]https://www.l2fater.cn 

## 许可证 - License

项目采用 "GNU GPLv3 "协议。
Project is under the `GNU GPLv3`.


## 捐助
如果您觉得我们的项目对您有所帮助，请扫下方二维码打赏我们一包华子。

![微信赞赏](https://foruda.gitee.com/images/1699409234265601529/fdd0eed0_1858155.jpeg "感谢您的赞赏")

## 联系
网站：
[https://www.l2fater.cn](https://www.l2fater.cn)(专注于天堂2单机玩家游戏体验的交流社区)

[https://www.17danji.cn](https://www.17danji.cn)（资源站，集成游戏，办公，娱乐等资源收录）

[https://17danji.cn](https://17danji.cn)(杀气丶自测，自编译网单-手游-页游-经验分享等)
邮箱:

51605539@qq.com

